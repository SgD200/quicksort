package quicksort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


public class Quicksort {

    public static void quicksort(int A[], int izq, int der) {

        int pivote=A[izq];
        int i=izq;
        int j=der;
        int aux;

        while(i<j){ 
           while(A[i]<=pivote && i<j) i++; 
           while(A[j]>pivote) j--;         
           if (i<j) {                                          
               aux= A[i];                  
               A[i]=A[j];
               A[j]=aux;
           }
         }
         A[izq]=A[j];
         A[j]=pivote;
         if(izq<j-1)
            quicksort(A,izq,j-1);
         if(j+1 <der)
            quicksort(A,j+1,der);
      }

    public static void main(String[] args) throws IOException{
        Scanner sc = new Scanner(System.in);
        int [] A = new int[9];
        for(int j=0;j<=8;j++){
            A[j] = sc.nextInt();
        }
        quicksort(A, 0, 8);
        System.out.println("-----------------");
        for(int j=0;j<=8;j++){
            System.out.println(A[j]);
        }
        
    }
    
}
